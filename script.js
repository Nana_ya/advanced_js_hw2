const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
];

let root = document.getElementById('root');
let newUl = document.createElement('ul');

function createP(message){
    let newP = document.createElement('span');
    newP.textContent = message;
    newP.innerHTML += "<br>";
    return newP;
}

function createList(obj){
    let newLi = document.createElement('li');
    try {
        if(obj.author === undefined) throw new Error ('Author. There is no attribute with such name in this object.');
        else if(obj.name === undefined) throw new Error ('Name. There is no attribute with such name in this object.');
        else if(obj.price === undefined) throw new Error ('Price. There is no attribute with such name in this object.');
        else {
            newLi.append(createP(`Author: ${obj.author}`));
            newLi.append(createP(`Name: ${obj.name}`));
            newLi.append(createP(`Price: ${obj.price}`));
            newUl.append(newLi);
        }
    }
    catch(err) {
        return err.message;
    }
}

books.forEach((arrValue, index) => {
    if (createList(arrValue) != undefined){
        console.log(`books[${index}]` + createList(arrValue))
    }
})

root.append(newUl);